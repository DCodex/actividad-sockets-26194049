# REQUERIMIENTOS DE SOFTWARE
* git V 2.29.* +
* nodejs  V  14.17.0 +
# INSTALAR
Desde consola ejecutar estos comandos.
Opcion automatica:
* npm i -g git+https://gitlab.com/DCodex/actividad-sockets-26194049.git
Opcion manual:
* git clone https://gitlab.com/DCodex/actividad-sockets-26194049.git
* cd actividad-sockets-26194049
* npm i -g .
# USO
Desde consola ejecutar estos comandos.
tcpclientsd -u nombre_usuario -h direccion_host -p puerto_tcp_host -s puerto_udp_usuario
# DESINSTALAR
Desde consola ejecutar estos comandos.
* npm un -g actividad-sockets-26194049
