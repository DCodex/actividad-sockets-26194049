#!/usr/bin/env node
let msg = '';
//UDP SOCKET
const net = require('net');
const udp = require('dgram');
const hasha = require('hasha');
// creating a client socket
const uSocket = udp.createSocket('udp4');
//PARAMETERS
const yargs = require("yargs");
const options = yargs
	.usage("Usage: -u <user> -h <host> -p <port> -s <uport>")
	.option("u", { alias: "user", describe: "username", type: "string", demandOption: true })
	.option("h", { alias: "host", describe: "host dir", type: "string", demandOption: true })
	.option("p", { alias: "port", describe: "access port", type: "number", demandOption: true })
	.option("s", { alias: "uPort", describe: "udp access port", type: "number", demandOption: true })
	.argv;

let stepCounter = -1;
let timeoutRetry = 0;
const msgSteps = (step) => {
	//console.log(step);
	switch (step) {
		case -1:
			socket.write(`helloiam ${options.user}`);
			break;
		case 0:
			socket.write(`msglen`);
			break;
		case 1:
			socket.write(`givememsg ${options.port + 1}`);
			break;
		case 2:
			//
			break;
		case 3:
			socket.write(`chkmsg ${hasha(msg, { algorithm: 'md5', encoding: 'hex' })}`);
			break;
		case 4:
			console.log(msg);
			socket.write(`bye`);
			break;
		case 5:
			socket.destroy();
			break;
	}
}
const handleTimeout = () => {
	if (timeoutRetry < 4) {
		//console.log(`Intento ${timeoutRetry + 1} ...`);
		timeoutRetry++;
		msgSteps(stepCounter);
		socket.setTimeout(5000, () => { handleTimeout() });
	}
	else {
		console.log('Intentos agotados...');
		socket.destroy();
	}
}

//SOCKET
const socket = new net.Socket();
//CONEXIÓN INICIAL SOCKET TCP
uSocket.bind(options.uPort, () => {
	//console.log('UDP CNT');
});

socket.connect(options.port, `${options.host}`, function () {
	msgSteps(stepCounter);
	//handleTimeout();
});

socket.setTimeout(5000, () => { handleTimeout() });

uSocket.on('message', function (content, rinfo) {
	let dMsg = Buffer.from(content.toString(), 'base64').toString();
	msg = dMsg;
	stepCounter++;
	msgSteps(stepCounter);
});

//CONTROL SUCCESS && ERROR
socket.on('data', function (data) {
	//socket.setKeepAlive
	const nData = data.toString();
	if (nData.startsWith("ok")) {
		stepCounter++;
		msgSteps(stepCounter);
	}
	else if (nData.startsWith("error")) {
		console.log(`Error caught at step : ${stepCounter}`);
		switch (stepCounter) {
			//TIMEOUT
			case -1:
				//USER
				console.log('Usuario inválido');
				break;
			case 0:
				//console.log('Error al obtener longitud de mensaje');
				console.log('Error en transferencia');
				break;
			case 3:
				//console.log('El checksum no coincide con el del mensaje enviado');
				console.log('Error en transferencia');
				break;
		}
		stepCounter = 4;
		socket.write('bye');
	}
});
socket.on('error', function (data) {
	//console.log(`Error at step : ${stepCounter}`);
	//console.log('Received: ' + data);
	if (data.errno === -4039) {
		console.log('Conexión no posible, intentelo en otro momento.');
	}
	else {
		switch (stepCounter) {
			case -1:
				//PUERTO 
				console.log('Puerto destino inválido');
				break;
		}
	}
	stepCounter = 4;
	socket.write('bye');
});
//CIERRE PARA SOCKET TCP
socket.on('close', function () {
	//console.log('Connection closed');
	uSocket.close();
});
//CIERRE PARA SOCKET UDP
uSocket.on('close', function () {
	//console.log('Connection closed for udp');
});
uSocket.on('error', function (err) {
	//console.log('Err udp ',err);
	console.log('Puerto destino del mensaje inválido');
});